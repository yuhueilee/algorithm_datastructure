def quick_sort(alist):
    '''
    Sort the input list in ascending order
      - Not stable (Since it performs swapping when doing partition)
      - In-place

    :param alist:       input list
    :return:            none
    :pre:               none
    :post:              input list is sorted
    :complexity:        best O(NlogN) where N is the length of the list
                        when the pivot divides the list in half in at least the first partition and
                        worst O(N^2) when pivot only reduce the list by 1 for each partition
    '''
    start = 0
    end = len(alist) - 1
    quick_sort_aux(alist, start, end)
    return alist


def quick_sort_aux(alist, start, end):
    '''
    Auxiliary function (Binary/ Direct/ Tail recursion)

    :param alist:       input list
    :param start:       start of the list
    :param end:         end of the list
    :return:            none
    :pre:               none
    :post:              input list is sorted
    :complexity:        best O(NlogN) where N is the length of the list
                        when the pivot divides the list in half in at least the first partition and
                        worst O(N^2) when pivot only reduce the list by 1 for each partition
    '''
    if start < end:
        # elaborate split: partition method
        boundary = partition(alist, start, end)
        # easy combination
        quick_sort_aux(alist, start, boundary-1)
        quick_sort_aux(alist, boundary+1, end)


def swap(alist, i, j):
    alist[i], alist[j] = alist[j], alist[i]


def partition(alist, start, end):
    '''
    Partition the list based on the pivot (middle element in the list)

    :param alist:       input list
    :param start:       start index
    :param end:         end index
    :return:            index where the pivot is relocated at in the list
    :pre:               none
    :post:              element chosen as pivot is allocated at the right place in list
    :complexity:        best and worst O(N) where N is from start to end
    '''
    # choose a pivot in the middle of the list
    mid = (start + end) //2
    pivot = alist[mid]

    # swap pivot with first element
    swap(alist, start, mid)

    # set up index: index = start
    index = start

    # do comparison from start (excl.) to end (incl.)
    for k in range(start+1, end+1):
        # index remain if element at k is greater than or equal to pivot
        # increase index then swap element at index with element at k
        if alist[k] < pivot:
            index += 1
            swap(alist, index, k)

    # after last swap, swap pivot with element at position index: pivot is in the correct position
    swap(alist, start, index)

    # return the boundary index
    return index


def test_quick_sort():
    print('Running test for quick sort')
    lst = [3, 4, 5, 6, 2, 1, 7, 8]
    assert quick_sort(lst) == sorted(lst)
    lst = [5, 7, 4, 8, 3, 9, 1]
    assert quick_sort(lst) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_quick_sort()
