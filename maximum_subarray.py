def max_subarray(a):
    '''
    Finds the maximum sum of all subarrays of a

    :param a:           list of integers
    :return:            maximum sum of all subarrays of input list
    :time complexity:   best and worst O(N) where N is the length of the input list
    :space complexity:  input size: O(N) where N is the length of the input list
                        auxiliary space: O(N) where N is the length of the input list
                        total space: O(N) where N is the length of the input list
    '''
    # create a memo list
    memo = [-1] * (len(a) + 1)

    # initialize the base case
    memo[0] = 0  # max sum 0 for empty list

    # solve the subproblems
    for i in range(1, len(memo)):
        memo[i] = max(a[i - 1], a[i - 1] + memo[i - 1])

    return max(memo)


if __name__ == '__main__':
    def test_max_subarray():
        assert max_subarray([1, 2, 3]) == 6
        assert max_subarray([1, -1, 2, 3]) == 5
        print('pass test for maximum subarray')


    test_max_subarray()
