'''
Unbounded knapsack returns the maximum value of item = [(v1, w1), ..., (vn, wn)]that can put in knapsack given the capacity constraint
Each item can be take more than once
    - Overlapping subproblems: Assume the max value is stored in MaxVal[i] where i<capacity
    - Optimal substructure: MaxVal[c] = max(value[k] + MaxVal[c-weight[k]])
'''


def unbounded_knapsack_btn(values, weights, capacity):
    '''
    Bottom up

    :param values:          list of item's value
    :param weights:         list of item's weight
    :param capacity:        integer capacity
    :return:                maximum value
    :time complexity:       best and worst O(NC) where N is the length of the values list and C is the capacity value
    :space complexity:      input size: best and worst O(N) where N is the length of the values list
                            auxiliary space: best and worst O(N) where N is the length of the values list
                            total space: best and worst O(N)
    '''
    # create a memo list
    memo = [0]*(capacity+1)
    # initialize the base case
    memo[0] = 0
    # solve the subproblems
    for i in range(1, len(memo)):
        # loop through the weights list
        for k in range(len(weights)):
            # if the capacity constraint is greater or equal to the item's weight
            if i >= weights[k]:
                tmp = values[k] + memo[i-weights[k]]
                if tmp > memo[i]:
                    memo[i] = tmp

    return memo[-1]


def unbounded_knapsack_top(values, weights, capacity):
    '''
    Top down

    :param values:          list of item's value
    :param weights:         list of item's weight
    :param capacity:        integer capacity
    :return:                maximum value
    :time complexity:       best and worst O(NC) where N is the length of the values list and C is the capacity value
    :space complexity:      input size: best and worst O(N) where N is the length of the values list
                            auxiliary space: best and worst O(N) where N is the length of the values list
                            total space: best and worst O(N)
    '''
    # create a memo list
    memo = [-1]*(capacity+1)
    # initialize the base case
    memo[0] = 0
    return unbounded_knapsack_aux(values, weights, capacity, memo)


def unbounded_knapsack_aux(values, weights, capacity, memo):
    # base case
    if capacity == 0:
        return memo[capacity]
    else:
        if memo[capacity] == -1:
            # initialize a max value
            max_value = -1
            # loop through the weights list
            for k in range(len(weights)):
                # if the capacity constraint is greater or equal to the item's weight
                if capacity >= weights[k]:
                    max_value = max(max_value, values[k] + unbounded_knapsack_aux(values, weights, capacity-weights[k], memo))
            if max_value == -1:
                memo[capacity] = memo[capacity-1]
            # assign the maximum to memo[capacity]
            memo[capacity] = max_value
        return memo[capacity]


if __name__ == '__main__':
    def test_unbounded_knapsack():
        values, weights = [550, 350, 180, 40], [9, 5, 6, 1]
        assert unbounded_knapsack_btn(values, weights, 12) == 780
        assert unbounded_knapsack_btn(values, weights, 6) == 390
        assert unbounded_knapsack_top(values, weights, 12) == 780
        assert unbounded_knapsack_top(values, weights, 6) == 390
        print('pass test for unbounded knapsack')


    test_unbounded_knapsack()