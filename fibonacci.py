'''
Compute the n th Fibonacci recursively
'''


def fib_recur(n):
    '''
    Recursion

    :param n:           n th fibonacci number
    :return:            n th fibonacci number
    :time complexity:   best and worst O(2^N) where N is the input number
    :space complexity:  input size: best and worst O(1)
                        auxiliary space: best and worst O(N) where N is the input number and the max depth of the recursive call is N
                        total space: best and worst O(N) where N is the input number
    '''
    # base case
    if n <= 1:
        return n
    # recursive
    else:
        return fib_recur(n-1) + fib_recur(n-2)


'''
Compute the n th Fibonacci number in dynamic programming
    - Overlapping subproblems: Assume we have computed and stored fib(i) for i<n
    - Optimal substructure: fib(n) = fib(n-1) + fib(n-2)
'''


def fib_dp_top(n):
    '''
    Top down

    :param n:           n th fibonacci number
    :return:            n th fibonacci number
    :time complexity:   best and worst O(N) where N is the input number as it calls for N times recursively
    :space complexity:  input size: best and worst O(1)
                        auxiliary space: best and worst O(N) where N is the size of the memo list and the max depth of the recursive call is N
                        total space: best and worst O(N) where N is the size of the memo list
    '''
    # create a memo list to store the n th fibonacci number
    memo = [-1] * (n + 1)
    # initialize the base case
    memo[0] = 0
    memo[1] = 1

    def fib_aux(n):
        # compute the n th fib number recursively
        if memo[n] == -1:
            memo[n] = fib_aux(n - 1) + fib_aux(n - 2)
        return memo[n]
    return fib_aux(n)


def fib_dp_btn(n):
    '''
    Bottom up

    :param n:           n th fibonacci number
    :return:            n th fibonacci number
    :time complexity:   best and worst O(N) where N is the input number as it loops through the list of size N
    :space complexity:  input size: best and worst O(1)
                        auxiliary space: best and worst O(N) where N is the size of the memo list
                        total space: best and worst O(N) where N is the size of the memo list
    '''
    memo = [None]*(n+1)
    memo[0] = 0
    memo[1] = 1
    for i in range(2, len(memo)):
        memo[i] = memo[i-1] + memo[i-2]
    return memo[n]


if __name__ == '__main__':
    def test_fib():
        fib_lst = [0, 1]
        for i in range(2, 13):
            fib_lst.append(fib_lst[i-1]+fib_lst[i-2])
        for i in range(1, len(fib_lst)):
            assert fib_lst[i] == fib_recur(i)
            assert fib_lst[i] == fib_dp_top(i)
            assert fib_lst[i] == fib_dp_btn(i)
        print('pass test for fibonacci')


    test_fib()
