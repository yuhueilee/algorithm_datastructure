from graph import *
import math


class Graph(Graph):
    def create_nodes(self, source):
        nodes = []
        for i in range(len(self)):
            # mark the distance of source vertex to be 0
            if i == source:
                self.vertices[i].dist = 0
            else:
                self.vertices[i].dist = math.inf
            vertex = self.vertices[i]
            nodes.append(Node(vertex.id, vertex.dist))
        return nodes

    def prim(self, source):
        '''
        Find the MST using Prim's algorithm - growing of trees
        (Similar to Dijkstra)

        :param source:          source vertex id
        :return:                predecessor list that forms the MST
        :time complexity:       best and worst O(ElogV) where V is the number of vertices and E is the number of edges
        :space complexity:      best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :aux space complexity:  best and worst O(V+E) where V is the number of vertices and E is the number of edges
        '''
        # reset the graph
        self.reset()
        # create a predecessor list
        pred = [None] * len(self)
        # create nodes
        nodes = self.create_nodes(source)
        # create a discovered min heap
        discovered = MinHeap(nodes, len(self))
        # mark the source vertex as discovered
        self.vertices[source].discovered = True

        # traverse through the graph
        while not discovered.is_empty():
            # serve from min heap
            node = discovered.serve()
            u = self.vertices[node.key]
            u.visited = True

            # traverse through the edges
            for e in u.edges:
                v = self.vertices[e.v]
                if not v.visited and not v.discovered:
                    v.dist = e.w  # use the edge weight
                    discovered.update(nodes[v.id], v.dist)
                    pred[v.id] = u.id
                    v.discovered = True
                elif not v.visited and v.discovered:
                    if v.dist > e.w:
                        v.dist = e.w  # use the edge weight
                        discovered.update(nodes[v.id], v.dist)
                        pred[v.id] = u.id

        return pred


if __name__ == '__main__':

    def test_prim():
        # create graph and insert edges (Tutorial question 1)
        g = Graph(7)
        edges = []
        edges.append((0, 1, 5))
        edges.append((0, 3, 2))
        edges.append((1, 2, 8))
        edges.append((1, 4, 1))
        edges.append((2, 4, 4))
        edges.append((3, 4, 16))
        edges.append((3, 5, 4))
        edges.append((4, 5, 8))
        edges.append((4, 6, 9))
        edges.append((5, 6, 10))
        g.insert_edges(edges, False)

        print(g)

        pred = g.prim(0)
        for v in pred:
            print(v)


    test_prim()
