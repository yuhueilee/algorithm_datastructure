'''
Invariant
 - Program will terminates after looping for N times where N is the length of the input list
 - Program will produce the correct result where items in input list are sorted
   - At k iteration, list[1:k] is sorted
'''


def insert(k, lst):
    '''
    Insert item at k index to the correct position in input list

    :param k:       index of the item to be inserted
    :param lst:     input list
    :return:        partially sorted list
    :time:          best O(1) when lst[k-1] <= lst[k] and worst O(N)
    :space:         best and worst O(N) where N is the length of the input list
    '''
    j = k  # indicate the range that need to be checked
    while j > 0 and lst[j - 1] > lst[j]:
        lst[j], lst[j - 1] = lst[j - 1], lst[j]
        j -= 1
    return lst


def insertion_sort(lst):
    '''
    Sort the input list in ascending order
      - Not stable
      - In-place

    :param lst:     input list
    :return:        sorted input list
    :time:          best O(N) when input list is sorted and worst O(N^2)
    :space:         best and worst O(N) where N is the length of the input list
    '''
    # declare lst[0] is sorted
    for i in range(1, len(lst)):
        insert(i, lst)
    return lst


def test_insertion_sort():
    print('Running test for insertion sort')
    lst = [3, 2, 6, 5, 4, 3, 2]
    assert insertion_sort(lst) == sorted(lst)
    lst = [3, 2, 5, 2, 4, 7, 4, 3, 5]
    assert insertion_sort(lst) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_insertion_sort()
