def house_profit(profits):
    '''
    Pick up the largest amount of profits from the row of houses,
    with the constraint that any two adjacent houses cannot be picked

    :param profits:     list containing row of coins
    :return:            largest amount of money that can be picked given the constraint
    :time complexity:   best and worst O(N) where N is the length of the input list
    :space complexity:  input size: O(N) where N is the length of the profits list
                        auxiliary space: O(N) where N is the length of the profits list
                        total space: O(N) where N is the length of the profits list
    '''
    # create a memo list
    memo = [0] * (len(profits) + 1)

    # initialize the base case
    memo[0] = 0
    memo[1] = profits[0]

    # solve the subproblems
    for i in range(2, len(memo)):
        exclude = memo[i - 1]
        include = memo[i - 2] + profits[i - 1]
        memo[i] = max(exclude, include)

    # find the optimal solution using backtracking
    sol = []
    i = len(memo) - 1
    while memo[i] != 0 and i > 0:
        if memo[i] != memo[i - 1]:
            sol.append(i - 1)
            i -= 2
        else:
            i -= 1

    return memo[-1], sorted(sol)


def house_profit_decision_arr(profits):
    # create a memo list
    memo = [0] * (len(profits) + 1)

    # initialize the base case
    memo[0] = 0
    memo[1] = profits[0]

    # create a decision array
    arr = [0] * (len(profits) + 1)

    # initialize the base case for arr
    arr[0] = 0
    arr[1] = profits[0]

    # solve the subproblems
    for i in range(2, len(memo)):
        exclude = memo[i - 1]
        include = memo[i - 2] + profits[i - 1]
        memo[i] = max(exclude, include)

        # store the last profit obtained from a subset of i houses
        if memo[i] == include:
            arr[i] = profits[i - 1]
        else:
            arr[i] = arr[i - 1]

    # create a optimal solution list
    sol = []

    # back track the optimal solution from the decision array
    max_profit = memo[-1]
    index = len(arr) - 1
    while max_profit > 0:
        p = arr[index]  # last profit included
        sol.append(p)
        max_profit -= p
        index = find_index_of(max_profit, memo)

    return memo[-1], sol


def find_index_of(value, memo):
    for i in range(len(memo) - 1, -1, -1):
        if memo[i] == value:
            return i


if __name__ == '__main__':
    def test_house_profit():
        assert house_profit([7, 2, 10, 12, 5]) == (22, [0, 2, 4])
        assert house_profit([50, 10, 12, 65, 40, 95, 100, 12, 20, 30]) == (252, [0, 3, 5, 7, 9])
        print('pass test for house profit')


    test_house_profit()
