def longest_increasing_subsequence(a):
    '''
    Finds the length of a longest increasing subsequence of a

    :param a:           list of integers
    :return:            length of a longest increasing subsequence of input list
    :time complexity:   best and worst O(N^2) where N is the length of the input list
    :space complexity:  input size: O(N) where N is the length of the input list
                        auxiliary space: O(N) where N is the length of the input list
                        total space: O(N) where N is the length of the input list
    '''
    # create a memo list
    memo = [-1] * (len(a) + 1)

    # initialize base case
    memo[0] = 0  # longest length 0 when list is empty

    # solve the subproblems
    for i in range(1, len(memo)):
        max_len = -1
        for k in range(i - 1, 0, -1):
            if a[i - 1] > a[k - 1]:
                if memo[k] > max_len:
                    max_len = memo[k]
        if max_len == -1:
            memo[i] = 1
        else:
            memo[i] = 1 + max_len

    return max(memo)


if __name__ == '__main__':
    def test_longest_increasing_subsequence():
        assert longest_increasing_subsequence([3, 4, 1, 5]) == 3
        assert longest_increasing_subsequence([0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]) == 6
        assert longest_increasing_subsequence([1, 12, 7, 0, 23, 11, 52, 31, 61, 69, 70, 2]) == 7
        print('pass test for longest increasing subsequence')


    test_longest_increasing_subsequence()
