'''
Invariant
 - Program will terminate when size of heap is 1
 - Program will produce correct result where items in input list are sorted
'''
from min_heap import *


def heap_sort(lst):
    '''
    Sort the input list in ascending order (when min heap is used)
      - Not stable
      - Not in-place (But able to become in-place)

    :param lst:     input list
    :return:        sorted input list
    :time:          best and worst O(NlogN)
    :space:         best and worst O(N) where N is the length of the input list
    '''
    # insert items in lst into min heap
    nodes = []
    for i in range(len(lst)):
        key, value = i, lst[i]
        node = Node(i, value)
        nodes.append(node)

    # create a min heap
    heap = MinHeap(nodes, len(lst))

    # extract min to form an ascending list
    res = []
    while not heap.is_empty():
        node = heap.serve()
        res.append(node.value)
    return res


def test_heap_sort():
    print('Running test for heap sort')
    lst = [3, 4, 5, 6, 2, 1, 7, 8]
    assert heap_sort(lst) == sorted(lst)
    lst = [5, 7, 4, 8, 3, 9, 1]
    assert heap_sort(lst) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_heap_sort()
