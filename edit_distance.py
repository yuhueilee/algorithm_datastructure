'''
Compute the minimum number of edit operations required to convert one sequence into another
    - Overlapping subproblems: Assume edit[i][j] stores the minimum number of operations required to convert s[1..i] to s[1..j] for i<n, j<m
    - Optimal substructure: edit[n][m] = min(edit[n-1]edit[m-1] + 1 if s[n] != s[m], edit[n-1][m] + 1, edit[n][m-1] + 1)
'''
import math


def edit_distance_btn(s1, s2):
    '''
    Bottom up

    :param s1:          original string
    :param s2:          string to be converted into
    :return:            minimum number of edit operations required to convert one sequence into another
    :time complexity:   best and worst O(NM) where N is the length of s1, M is the length of s2
    :space complexity:  input size: O(N+M) where N is the length of s1, M is the length of s2
                        auxiliary space: O(NM) where N is the length of s1, M is the length of s2
                        total space: O(NM + N + M)
    '''
    # create a memo of two dimension
    memo = [None] * (len(s1) + 1)
    for i in range(len(memo)):
        memo[i] = [math.inf] * (len(s2) + 1)

    # initialize the base case
    for i in range(len(s1) + 1):
        # cost of converting s1[1..i] into nothing
        memo[i][0] = i
    for j in range(len(s2) + 1):
        # cost of converting nothing into s2[1..j]
        memo[0][j] = j

    # solve the subproblems
    for i in range(1, len(memo)):
        for j in range(1, len(memo[i])):
            # initialize a min cost
            min_cost = -1
            # no need replacement
            if s1[i - 1] == s2[j - 1]:
                min_cost = memo[i - 1][j - 1]
            # need to replace the last character
            else:
                min_cost = min(memo[i - 1][j - 1] + 1, memo[i - 1][j] + 1, memo[i][j - 1] + 1)
            # assign min cost to memo[i][j]
            memo[i][j] = min_cost

    return memo[-1][-1]


def edit_distance_top(s1, s2):
    '''
    Top down

    :param s1:          original string
    :param s2:          string to be converted into
    :return:            minimum number of edit operations required to convert one sequence into another
    :time complexity:   best and worst O(NM) where N is the length of s1, M is the length of s2
    :space complexity:  input size: O(N+M) where N is the length of s1, M is the length of s2
                        auxiliary space: O(NM) where N is the length of s1, M is the length of s2
                        total space: O(NM + N + M)
    '''
    # create a memo of two dimension
    memo = [None] * (len(s1) + 1)
    for i in range(len(memo)):
        memo[i] = [math.inf] * (len(s2) + 1)

    # initialize the base case
    for i in range(len(s1) + 1):
        # cost of converting s1[1..i] into nothing
        memo[i][0] = i
    for j in range(len(s2) + 1):
        # cost of converting nothing into s2[1..j]
        memo[0][j] = j

    # solve the subproblems
    min_distance = edit_distance_aux(len(s1), len(s2), s1, s2, memo)

    return min_distance


def edit_distance_aux(i, j, s1, s2, memo):
    if i == 0 or j == 0:
        return memo[i][j]
    else:
        if memo[i][j] == math.inf:
            min_value = math.inf
            if s1[i - 1] == s2[j - 1]:
                min_value = edit_distance_aux(i - 1, j - 1, s1, s2, memo)
            else:
                min_value = 1 + min(edit_distance_aux(i, j - 1, s1, s2, memo),
                                    edit_distance_aux(i - 1, j, s1, s2, memo),
                                    edit_distance_aux(i - 1, j - 1, s1, s2, memo))
            memo[i][j] = min_value
        return memo[i][j]


if __name__ == '__main__':
    def test_edit_distance_btn():
        assert edit_distance_btn('eat', 'cat') == 1  # Replace e with c
        assert edit_distance_btn('sings', 'shine') == 3  # Insert h at position 2, replace g with e, delete s
        assert edit_distance_btn('happy', 'apply') == 2  # Delete h, insert l at position 4
        assert edit_distance_top('eat', 'cat') == 1  # Replace e with c
        assert edit_distance_top('sings', 'shine') == 3  # Insert h at position 2, replace g with e, delete s
        assert edit_distance_top('happy', 'apply') == 2  # Delete h, insert l at position 4
        print('pass test for edit distance')


    test_edit_distance_btn()
