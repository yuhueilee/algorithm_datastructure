class Trie:
    class Node:
        def __init__(self, size=27, freq=None, uni_freq=None, data=None):
            """
            Construct a node object that contains:
             - an array of size alphabet including '$' as terminator
             - freq stores the total number of strings at that node
             - uni_freq stores the total number of unique strings at that node
             - data payload stores the corresponding index to the input list of string
            """
            # array of size alphabet
            self.link = [None] * size
            # frequency of the node
            self.freq = freq
            # unique frequency of the node
            self.uni_freq = uni_freq
            # data payload
            self.data = data

    def __init__(self, text):
        """
        Pre-process text into the trie

        Precondition:           input text must not contain empty string
                                input text contains only lowercase alphabet characters
                                input text may be empty list
                                input text may contain duplicates
        Arguments:              text = a list of strings
        Time complexity:        Best case O(T) where T is the total number of characters in the input text list
                                Worst case O(T) where T is the total number of characters in the input text list
        Space complexity:       Best and worst O(T) where T is the total number of characters in the input text list
        Aux space complexity:   Best and worst O(T) where T is the total number of characters in the input text list
        Return:                 None
        """
        # create a root node
        self.root = self.Node(freq=0, uni_freq=0, data=[])
        # insert the strings in text into the trie
        data = list(range(len(text)))
        i = 0
        for string in text:
            self[string] = data[i]
            i += 1

    def __setitem__(self, key, value):
        """
        Add word into the Trie

        Precondition:           input string key must not be empty
        Arguments:              key = string to be inserted
                                value = value of the key
        Time complexity:        Best case O(M) where M is the number of characters in string key
                                Worst case O(M) where M is the number of characters in string key
        Space complexity:       Best and worst O(T) where T is the total number of characters in the trie
        Aux space complexity:   Best and worst O(T) where T is the total number of characters in the trie
        Return:                 None
        """
        # start from the root
        current = self.root
        # insert the string into the trie
        # update the unique frequency
        current.uni_freq += self.insert_recur_aux(current, key, 0, value)
        # update the frequency
        current.freq += 1
        # insert the data
        current.data.append(value)

    def insert_recur_aux(self, current, key, pointer, value=None):
        """
        Add word into the Trie recursively

        Precondition:           input string key must not be empty
        Arguments:              current = current node
                                key = string to be inserted
                                pointer = points to the character in string key
                                value = value of the key
        Time complexity:        Best case O(M) where M is the number of characters in string key
                                Worst case O(M) where M is the number of characters in string key
        Space complexity:       Best and worst O(T) where T is the total number of characters in the trie
        Aux space complexity:   Best and worst O(T) where T is the total number of characters in the trie
        Return:                 0 if the string is not unique, 1 otherwise
        """
        # base case
        if pointer == len(key):
            index = 0
            exist = False
            # if terminator '$' exists
            if current.link[index] is not None:
                # traverse to the next node
                current = current.link[index]
                exist = True
            else:
                # create a new node
                current.link[index] = self.Node(freq=0, uni_freq=0, data=[])
                # traverse to the next node
                current = current.link[index]
                # update the unique frequency
                current.uni_freq += 1
            # update the frequency
            current.freq += 1
            # insert the data
            current.data.append(value)
            # return the unique frequency
            if exist:
                return 0
            else:
                return 1
        # else
        else:
            # compute the index of the char
            index = ord(key[pointer]) - 97 + 1
            # if char exists
            if current.link[index] is not None:
                # travers to the next node
                current = current.link[index]
            # else
            else:
                # create a new node
                current.link[index] = self.Node(freq=0, uni_freq=0, data=[])
                # travers to the next node
                current = current.link[index]
            # update the frequency
            current.freq += 1
            # insert the data
            current.data.append(value)
            # recurse down the trie
            uni_freq = self.insert_recur_aux(current, key, pointer + 1, value)
            # update the unique frequency
            current.uni_freq += uni_freq
            # return the unique frequency
            return uni_freq