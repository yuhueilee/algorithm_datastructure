def quick_sort(lst, keys):
    '''
    Sort the input list in ascending order
      - Not stable
      - In-place

    :param lst:         list of items
    :param keys:        key list to be sorted
    :return:            sorted input list
    :time:              best O(NlogN) and worst O(N^2) where N is the length of the list
    :space:             best and worst O(N) where N is the length of the list
    '''
    start, end = 0, len(lst) - 1
    quick_sort_aux(lst, keys, start, end)
    return lst


def quick_sort_aux(lst, keys, start, end):
    if start < end:
        # call partition
        blue_boundry, red_boundry = partition(lst, keys, start, end)
        # call auxiliary function
        quick_sort_aux(lst, keys, start, blue_boundry - 1)
        quick_sort_aux(lst, keys, red_boundry + 1, end)


def partition(lst, keys, start, end):
    '''
    Using Dutch National Flag method
      - Blue: less than pivot
      - White: equal to pivot
      - Red: greater than pivot

    :param lst:         list of items
    :param keys:        key list to be sorted
    :param start:       start position
    :param end:         end position
    :return:            boundries of the list that has been sorted in-between
    :time complexity:   best and worst O(end-start)
    '''
    # choose a pivot
    index = (start + end) // 2
    pivot = keys[index]
    # initialize the pointer
    j = start
    # traverse through the list
    while j <= end:
        if keys[j] < pivot:
            swap(keys, start, j)
            swap(lst, start, j)
            start += 1
            j += 1
        elif keys[j] > pivot:
            swap(keys, j, end)
            swap(lst, j, end)
            end -= 1
        else:
            j += 1

    return start, end


def swap(lst, i, j):
    lst[i], lst[j] = lst[j], lst[i]


def test_quick_sort():
    print('Running test for quick sort')
    lst, keys = [3, 4, 5, 6, 2, 1, 7, 8], [3, 4, 5, 6, 2, 1, 7, 8]
    assert quick_sort(lst, keys) == sorted(lst)
    lst, keys = [5, 7, 4, 8, 3, 9, 1], [5, 7, 4, 8, 3, 9, 1]
    assert quick_sort(lst, keys) == sorted(lst)
    lst, keys = [1, 1, 1, 2, 2, 2, 1, 7, 7, 3, 3, 4, 2, 3], [1, 1, 1, 2, 2, 2, 1, 7, 7, 3, 3, 4, 2, 3]
    assert quick_sort(lst, keys) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_quick_sort()
