def merge_sort(alist):
    '''
    Sort the input list in ascending order
      - Stable
      - Not in-place

    :param alist:       list to be sorted
    :return:            none
    :pre:               none
    :post:              sorted list in ascending order
    :time:              best and worst O(NlogN) where N is from start to end + 1
    :space:             best and worst O(NlogN) = O(N) local space usage * O(logN) maximum depth of recursion
    '''
    temp = [None] * len(alist)
    start = 0
    end = len(alist) - 1
    merge_sort_aux(alist, start, end, temp)
    return alist


def merge_sort_aux(alist, start, end, temp):
    '''
    Split the input list into half, and merge and sort it on the way back,
    then copy from temporary list to the input list
    (Binary/ Direct/ Non-tail recursion)

    :param alist:       list to be sorted
    :param start:       position index to start
    :param end:         position index to end
    :param temp:        temporary list
    :return:            none
    :complexity:        best and worst O(NlogN) where N is from start to end + 1
    '''
    # base case
    if start < end:
        mid = (start + end) // 2
        # split the list
        merge_sort_aux(alist, start, mid, temp)
        merge_sort_aux(alist, mid + 1, end, temp)
        # combine the list
        merge_array(alist, start, mid, end, temp)
        # copy from temp to alist
        for i in range(start, end + 1):
            alist[i] = temp[i]


def merge_array(alist, start, mid, end, temp):
    '''
    Merging two sub-lists and sort them in ascending order

    :param alist:       list to be sorted
    :param start:       position index to start
    :param mid:         mid position index of start to end
    :param end:         position index to end
    :param temp:        temporary list
    :return:            none
    :complexity:        best and worst O(N) where N from start to end + 1
    '''
    i = start
    j = mid + 1
    for k in range(start, end + 1):
        if i > mid:  # left finished, copy right
            temp[k] = alist[j]
            j += 1
        elif j > end:  # right finished, copy left
            temp[k] = alist[i]
            i += 1
        elif alist[i] <= alist[j]:
            temp[k] = alist[i]
            i += 1
        else:
            temp[k] = alist[j]
            j += 1


def test_merge_sort():
    print('Running test for merge sort')
    lst = [3, 4, 5, 6, 2, 1, 7, 8]
    assert merge_sort(lst) == sorted(lst)
    lst = [5, 7, 4, 8, 3, 9, 1]
    assert merge_sort(lst) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_merge_sort()
