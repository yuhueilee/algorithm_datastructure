def longest_common_subsequence(a, b):
    '''
    Finds the length of a longest common subsequence of a and b

    :param a:               list of integers
    :param b:               list of integers
    :return:                longest common subsequence of a and b
    :time complexity:       best and worst O(NM) where N is the length of the input a list, M is the length of the input b list
    :space complexity:      input size: O(N+M) where N is the length of the input a list, M is the length of the input b list
                            auxiliary space: O(NM)  where N is the length of the input a list, M is the length of the input b list
                            total space: O(NM) where N is the length of the input a list, M is the length of the input b list
    '''
    # create a memo list
    memo = [None]*(len(a)+1)
    for i in range(len(memo)):
        memo[i] = [-1]*(len(b)+1)

    # initialize the base case
    memo[0][0] = 0 # 0 length between empty list and empty list
    for i in range(1, len(a)+1):
        memo[i][0] = 0 # 0 length between a[:i] and empty list
    for j in range(1, len(b)+1):
        memo[0][j] = 0 # 0 length between empty list and b[:j]

    # solve the subproblems
    for i in range(1, len(memo)):
        for j in range(1, len(memo[i])):
            if a[i-1] == b[j-1]:
                memo[i][j] = 1 + memo[i-1][j-1]
            else:
                memo[i][j] = max(memo[i-1][j-1], memo[i][j-1], memo[i-1][j])

    return memo[-1][-1]


if __name__ == '__main__':
    def test_longest_common_subsequence():
        assert longest_common_subsequence([2, 3, 4], [1, 3, 4]) == 2
        assert longest_common_subsequence([1, 2, 3, 4], [2, 3]) == 2
        assert longest_common_subsequence([1, 5, 2, 3, 8, 9], [1, 5, 4, 3, 8, 9]) == 5

        print('pass test for longest common subsequence')

    test_longest_common_subsequence()

