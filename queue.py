'''
Invariant:
 - valid data in the front to rear-1 positions
'''


class Queue:
    def __init__(self, max_capacity):
        '''
        Create an circular queue
        Elements:   array - store the items
                    integer - referring to the first element to be served
                    integer - referring to the first empty slot at the rear
                    integer - count the numbers of items in queue

        :param max_capacity:    maximum capacity of the queue
        :pre:                   size must be positive
        :post:                  an instance of Queue is created
        :complexity:            best and worst O(n) where n is the size of the queue
        '''
        if max_capacity <= 0:
            raise Exception("The size must be positive")
        self.array = [None]*max_capacity
        self.front = 0
        self.rear = 0
        self.count = 0

    def size(self):
        '''
        Return the size of the queue

        :return:        size of the queue
        :pre:           none
        :post:          none
        :complexity:    best and worst O(1)
        '''
        return self.count

    def is_full(self):
        '''
        Check if the queue is full or not

        :return:        True if full, False otherwise
        :pre:           none
        :post:          none
        :complexity:    best and worst O(1)
        '''
        return self.count >= len(self.array)

    def is_empty(self):
        '''
        Check if the queue is empty or not

        :return:        True if empty, False otherwise
        :pre:           none
        :post:          none
        :complexity:    best and worst O(1)
        '''
        return self.size() == 0

    def append(self, item):
        '''
        Append the item to the rear of the queue

        :param item:    item to be appended
        :return:        True if appended, False otherwise
        :pre:           queue must not be full
        :post:          item being appended
        :complexity:    best and worst O(1)
        '''
        if self.is_full():
            raise Exception("queue is full")
        self.array[self.rear] = item
        self.rear = (self.rear + 1) % len(self.array)
        self.count += 1
        return self.is_full()

    def serve(self):
        '''
        Serve the front item in queue

        :return:        item being served
        :pre:           queue must not be empty
        :post:          item being served
        :complexity:    best and worst O(1)
        '''
        if self.is_empty():
            raise Exception("queue is empty")
        served_item = self.array[self.front]
        self.front = (self.front + 1) % len(self.array)
        self.count -= 1
        return served_item

    def reset(self):
        '''
        Remove all elements from the container

        :return:        none
        :pre:           none
        :post:          none
        :complexity:    best and worst O(1)
        '''
        self.front = 0
        self.rear = 0
        self.count = 0