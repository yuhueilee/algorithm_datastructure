'''
Max Heap (array):
 - root is always greater than its child
 - essentially complete binary tree
 - duplicate keys are allowed

Invariants:
 - tree is a binary tree
 - essentially complete
 - heap sorted
'''


class MaxHeap:

    def __init__(self, size):
        '''
        Create a max heap instance
            - leave 0 index in the array

        :param size:    size of the array which is the number of nodes in max heap
        :pre:           size must be of positive integer
        :post:          max heap instance is created
        :complexity:    best and worst O(N) where N is the size of the array
        '''
        self.array = [None] * size
        self.counter = 0

    def is_empty(self):
        '''
        Check if the max heap is empty or not

        :return:        True if empty, False otherwise
        :pre:           none
        :post:          True is returned if heap is empty, False otherwise
        :complexity:    best and worst O(1)
        '''
        return self.counter == 0

    def __str__(self):
        '''
        Return a string representation of the max heap

        :return:        string representation of the max heap
        :pre:           none
        :post:          string representation of the max heap is returned
        :complexity:    best and worst O(N) where N is the size of the array
        '''
        temp = str(self.array[0])
        for i in range(1, self.counter + 1):
            temp += "," + str(self.array[i])

        return temp

    def add(self, key, data):
        '''
        Add the item with (key, data) into the max heap
        Steps:  1. add the element to the bottom of the tree
                2. increment the counter
                3. rise the element until the right position

        :param key:     key of the item
        :param data:    data of the item
        :return:        none
        :pre:           none
        :post:          node is inserted to the max heap and locate at the right position
        :complexity:    best O(1) when no need to rise and worst O(logN) depends on the depth of the max heap
        '''
        # check if there is space in the array
        if self.counter + 1 < len(self.array):
            self.array[self.counter+1] = (key, data)
        # resize the array if it is full
        else:
            # self.resize()
            raise Exception("heap is full")

        # increment counter
        self.counter += 1
        # swap the node to the right place
        self.rise(self.counter)

    def rise(self, k):
        '''
        Swap the node to the right position in array

        :param k:       index k
        :return:        none
        :pre:           k must be of valid index
        :post:          node at position k is rearranged to right position
        :complexity:    best O(1) when parent of node at position k is greater than node and
                        worst O(logN) where N is the number of nodes and depends on the depth of the max heap
                        (when element rises all the way to the top)
        '''
        # if k is not pointing to the root and key of child is greater than key of parent
        while k > 1 and self.array[k][0] > self.array[k//2][0]:
            # swap the parent and child
            self.swap(k, k//2)
            # decrement index k
            k = k//2

    def get_max(self):
        '''
        Get the maximum node in the bst
        Steps:  1. swap the root with the last element
                2. extract the node
                3. decrement the counter
                4. sink the new root node to the right position

        :return:        maximum node
        :pre:           none
        :post:          none
        :complexity:    best O(1) when no need to sink and worst O(logN) depends on the depth of the max heap
        '''
        # memorized the root
        to_be_returned = self.array[1]
        # swap the root with the last node
        self.swap(1, self.counter)
        # decrement the counter
        self.counter -= 1
        # sink the new root to the right position
        self.sink(1)

        return to_be_returned

    def sink(self, k):
        '''
        Sink the node at index k to the right position in array

        :param k:       integer k
        :return:        none
        :pre:           none
        :post:          the node at index k is sinked to the right position in max heap
        :complexity:    best O(1) when swap is not needed and worst O(logN) when it swap until the bottom of the max heap
        '''
        # check if the node has at least a left child
        while 2*k <= self.counter:
            # find the index of the largest child of node k
            child = self.largest_child(k)

            # no need to sink when parent is greater than the largest child
            if self.array[k][0] >= self.array[child][0]:
                break

            # sink the node: swap the k parent and the child
            self.swap(k, child)

            # update the index k
            k = child

    def largest_child(self, k):
        '''
        Return the index of the largest child of parent at index k

        :param k:       integer k
        :return:        index of the largest child
        :pre:           none
        :post:          the index of the largest child for parent at index k is returned
        :complexity:    best and worst O(1)
        '''
        # check whether the left child is the last node in max heap
        # check whether the key of the left child is greater than the right child
        if 2*k == self.counter or self.array[2*k][0] > self.array[2*k+1][0]:
            return 2*k
        else:
            return 2*k + 1

    def swap(self, i, j):
        '''
        Swap the node at i and j

        :param i:       index i
        :param j:       index j
        :return:        none
        :pre:           i, j must be valid index
        :post:          item at index i is swapped with item at index j
        :complexity:    best and worst O(1)
        '''
        self.array[i], self.array[j] = self.array[j], self.array[i]


def heap_sort(alist):
    '''
    Sort the input list in order using max heap

    :param alist:       input list
    :return:            none
    :pre:               elements in list must be comparable
    :post:              prints out the elements in list starting from the largest element
    :complexity:        best and worst O(NlogN) where N is the number of elements in list
                            - add: O(logN) * N times
                            - get max: O(logN) * N times
    '''
    # create a max heap
    max_heap = MaxHeap(len(alist)+1)
    # push the element inside the heap
    for item in alist:
        max_heap.add(item, "temp_data")
    # extract the maximum node in the heap
    res = []
    while not max_heap.is_empty():
        res.append(max_heap.get_max()[0])
    return res


def verify_heap(arr):
    '''
    Verify whether the input array is a max heap or not

    :param arr:     array containing comparable elements
    :return:        True if verified, False otherwise
    :pre:           array must containing elements that are comparable
    :post:          boolean is returned indicating whether the input array is a max heap or not
    :complexity:    best O(1) when at the first level it breaks the heap-ordered property and
                    worst O(N//2) where N is the length of the input array
    '''
    k = 0
    while 2*k + 1 < len(arr):
        largest_child = max(arr[2*k], arr[2*k +1])

        if arr[k] < largest_child:
            return False
        k += 1

    return True


def test_max_heap():
    mx = MaxHeap(6)
    mx.add(2, "data1")
    mx.add(5, "data2")
    mx.add(11, "data3")
    mx.add(24, "data4")
    mx.add(17, "data5")

    # print(mx)

    alist = [5, 2, 78, 90, 3, 2, 1, 5, 8]
    sol = heap_sort(alist)

    print(verify_heap(sol))


if __name__ == '__main__':
    test_max_heap()







