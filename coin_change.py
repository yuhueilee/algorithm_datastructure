'''
Find the minimum number of coins that add up to a given value V where the denominations = {a1, .., an} in descending order
    - Overlapping subproblems: Assume the minimum number of coins that adds up to value i is stored in MinCoins[i] where i<v
    - Optimal substructure: MinCoins[v] = 1 + min(MinCoins[v-k]) for k in denominations
'''
import math


def coin_exchange_btn(v, deno):
    '''
    Bottom up

    :param v:           value
    :param deno:        denominators of coins
    :return:            minimum number of coins that add up to value v
    :time complexity:   best and worst O(VN) where V is the input value and N is the number of coins in the denominators
    :space complexity:  input size: best and worst O(N) where N is the length of the denominators
                        auxiliary space: best and worst O(V) where V is the input value
                        total space: best and worst O(N+V)
    '''
    # create a memo list of size v+1
    memo = [math.inf]*(v+1)
    # initialize the base case
    memo[0] = 0
    # solve the subproblems
    for i in range(1, v+1):
        # check the coins in the denominator
        for c in deno:
            # if the value is greater or equal to the coin
            if i >= c:
                tmp = 1 + memo[i-c]
                # let memo[i] be tmp if tmp the smaller than memo[i]
                if tmp < memo[i]:
                    memo[i] = tmp

    return memo[-1]


def coin_exchange_top(v, deno):
    '''
    Top down

    :param v:           value
    :param deno:        denominators of coins
    :return:            minimum number of coins that add up to value v
    :time complexity:   best and worst O(VN) where V is the input value and N is the number of coins in the denominators
    :space complexity:  input size: best and worst O(N) where N is the length of the denominators
                        auxiliary space: best and worst O(V) where V is the input value
                        total space: best and worst O(N+V)
    '''
    # create a memo list
    memo = [-1]*(v+1)
    # initialize the base case
    memo[0] = 0
    return coin_exchange_aux(v, deno, memo)


def coin_exchange_aux(v, deno, memo):
    # base case
    if v == 0:
        return 0
    else:
        # if memo[v] has not yet been computed
        if memo[v] == -1:
            # initialize min_coin to infinity
            min_coin = math.inf
            # loop through the coins in denominators
            for i in range(len(deno)):
                # if coin is less than or equal to value v
                if v >= deno[i]:
                    # find the minimum among all the possible combinations
                    min_coin = min(min_coin, coin_exchange_aux(v-deno[i], deno, memo)+1)
            # assign the minimum to memo[v]
            memo[v] = min_coin
        return memo[v]


if __name__ == '__main__':
    def test_min_coins():
        deno = [9, 5, 6, 1]
        deno2 = [5, 7, 8]
        deno3 = [2, 5, 7]
        assert coin_exchange_btn(0, deno) == 0
        assert coin_exchange_btn(12, deno) == 2
        assert coin_exchange_btn(11, deno) == 2
        assert coin_exchange_btn(12, deno2) == 2
        assert coin_exchange_btn(13, deno3) == 4

        assert coin_exchange_top(0, deno) == 0
        assert coin_exchange_top(12, deno) == 2
        assert coin_exchange_top(11, deno) == 2
        assert coin_exchange_top(12, deno2) == 2
        assert coin_exchange_top(12, deno2) == 2
        assert coin_exchange_top(13, deno3) == 4
        print('pass test for coin exchange')


    test_min_coins()



