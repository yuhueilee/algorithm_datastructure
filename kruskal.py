from graph import *
from quick_sort_hoare import *


class Graph(Graph):

    def kruskal(self):
        '''
        Find the MST using Kruskal's algorithm - merging of trees

        :return:                predecessor list that forms the MST
        :time complexity:       best and worst O(ElogV) where V is the number of vertices and E is the number of edges
        :space complexity:      best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :aux space complexity:  best and worst O(V+E) where V is the number of vertices and E is the number of edges
        '''
        # create a edges list
        edges, keys = [], []
        for v in self.vertices:
            edges += v.edges
            for e in v.edges:
                keys.append(e.w)
        # sort the edges by weight
        sorted_edges = quick_sort(edges, keys)
        # create union-find data structure
        parent = [-1] * len(self)
        # create a tree with all the vertices without the edges
        tree = Graph(len(self))

        # traverse through the sorted edge and perform union find
        edges = []
        for e in sorted_edges:
            u, v = e.u, e.v
            if self.find(u, parent) != self.find(v, parent):
                edges.append((e.u, e.v, e.w))
                # union set of u with set of v
                self.union(u, v, parent)
        tree.insert_edges(edges, False)
        return tree

    def find(self, target, parent):
        '''
        Find the target vertex's parent id and return

        :param target:          target vertex id
        :param parent:          parent list of the vertex id
        :return:                parent vertex id of the target vertex
        :time complexity:       best and worst O(logV) where V is the number of vertices
        :space complexity:      best and worst O(V) where V is the number of vertices
        :aux space complexity:  best and worst O(1)
        '''
        while parent[target] >= 0:
            target = parent[target]
        return target

    def union(self, u, v, parent):
        '''
        Union set of u and set of v where the smaller tree is merged to the bigger tree

        :param u:               vertex id
        :param v:               vertex id
        :param parent:          list of parent
        :return:                None
        :time complexity:       best and worst O(logV) where V is the number of vertices
        :space complexity:      best and worst O(V) where V is the number of vertices
        :aux space complexity:  best and worst O(1)
        '''
        # check the size of the tree
        if (-1 * parent[self.find(u, parent)]) > (-1 * parent[self.find(v, parent)]):
            root, child = u, v
        else:
            root, child = v, u
        # union the child to the root
        cp_index, rp_index = self.find(child, parent), self.find(root, parent)
        # update the size of the root parent
        parent[rp_index] += parent[cp_index]
        # change the child parent to be the root
        parent[cp_index] = root


def test_kruskal():
    print('Running test')
    # create graph and insert edges (Tutorial question 1)
    g = Graph(7)
    edges = []
    edges.append((0, 1, 5))
    edges.append((0, 3, 2))
    edges.append((1, 2, 8))
    edges.append((1, 4, 1))
    edges.append((2, 4, 4))
    edges.append((3, 4, 16))
    edges.append((3, 5, 4))
    edges.append((4, 5, 8))
    edges.append((4, 6, 9))
    edges.append((5, 6, 10))
    g.insert_edges(edges, False)

    tree = g.kruskal()
    print(tree)

    print('pass all tests')


test_kruskal()
