from graph import *


class Graph(Graph):
    def dfs(self, source):
        '''
        Depth-First Search (single source, multiple targets) for unweighted graph
            - Reachability

        :param source:          source vertex
        :return:                list of vertices in dfs sequence
        :time complexity:       best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :space complexity:      best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :aux space complexity:  best and worst O(V+E) where V is the number of vertices and E is the number of edges
        '''
        # reset the graph
        self.reset()
        # create a list to store visited vertices
        visited = []
        # create a stack to store discovered vertices
        discovered = []
        # push source vertex to discovered
        source = self.vertices[source]
        discovered.append(source)
        # mark it as discovered
        source.discovered = True

        # traverse through the graph
        while len(discovered) != 0:
            u = discovered.pop()  # pop from the stack
            u.visited = True  # mark it as visited
            visited.append(u)  # push to visited

            # traverse through edges
            for e in u.edges:
                v = e.v
                v = self.vertices[v]
                # check if neighbor discovered and visited or not
                if not v.visited and not v.discovered:
                    v.discovered = True  # mark it as discovered
                    discovered.append(v)  # push to discovered

        return visited


if __name__ == '__main__':
    def test_graph(n, edges):
        # create a graph
        # vertex id = 0, 1, 2, ..., n
        my_graph = Graph(n)
        print('initialize the graph:\n', my_graph)
        my_graph.insert_edges(edges)
        print('inserting edges to the graph:\n', my_graph)

        return my_graph


    # dfs
    def test_dfs(graph):
        print('DFS:')
        for i in range(len(graph)):
            print(str(i) + " reaches " + str(len(graph.dfs(i))))


    # create a list of edges
    edges = []
    edges.append((0, 1, 5))  # u = 0, v = 1, w = 5
    edges.append((0, 2, 12))  # u = 0, v = 2, w = 12
    edges.append((1, 2, 6))  # u = 1, v = 2, w = 6
    edges.append((2, 3, 1))  # u = 2, v = 3, w = 1
    edges.append((3, 4, 7))  # u = 3, v = 4, w = 7
    edges.append((4, 1, 3))  # u = 4, v = 1, w = 3
    my_graph = test_graph(5, edges)
    test_dfs(my_graph)
