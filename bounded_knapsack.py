'''
1/0 knapsack returns the maximum value of item = [(v1, w1), ..., (vn, wn)]that can put in knapsack given the capacity constraint
Each item can only be taken once
Since we need to make sure the item we picked did not include in the optimal subsolution, hence we need two axes
    - Overlapping subproblems: Assume the max value is stored in MaxVal[i][j] where i<number of items and j<capacity
    - Optimal substructure: MaxVal[c] = max(values[k] + MaxVal[i-1][j-weights[k]], MaxVal[i-1][j])
'''


def knapsack_btn(values, weights, capacity):
    '''
    Bottom up

    :param values:          list of item's value
    :param weights:         list of item's weight
    :param capacity:        integer capacity
    :return:                maximum value
    :time complexity:       best and worst O(NC) where N is the length of the values list and C is the capacity value
    :space complexity:      input size: best and worst O(N) where N is the length of the values list
                            auxiliary space: best and worst O(NC) where N is the length of the values list and C is the capacity
                            total space: best and worst O(NC)
    '''
    # create a memo list with i th rows and j th columns where i is the number of items, j is the capacity
    memo = [None] * (len(values) + 1)
    for i in range(len(memo)):
        memo[i] = [0] * (capacity + 1)
    # initialize the base case
    for i in range(len(values) + 1):
        memo[i][0] = 0
    for j in range(capacity + 1):
        memo[0][j] = 0
    # solve the subproblems
    for i in range(1, len(memo)):
        # for each capacity constraint
        for j in range(1, len(memo[i])):
            # if the capacity is greater or equal to the item's weight
            if j >= weights[i - 1]:
                # find the maximum among include the item or do not include the item
                memo[i][j] = max(values[i - 1] + memo[i - 1][j - weights[i - 1]], memo[i - 1][j])
            else:
                # set the value to not include the item
                memo[i][j] = memo[i - 1][j]

    return memo[-1][-1]


def knapsack_top(values, weights, capacity):
    '''
    Top down

    :param values:          list of item's value
    :param weights:         list of item's weight
    :param capacity:        integer capacity
    :return:                maximum value
    :time complexity:       best and worst O(NC) where N is the length of the values list and C is the capacity value
    :space complexity:      input size: best and worst O(N) where N is the length of the values list
                            auxiliary space: best and worst O(NC) where N is the length of the values list and C is the capacity
                            total space: best and worst O(NC)
    '''
    # create a memo list with i th rows and j th columns where i is the number of items, j is the capacity
    memo = [None] * (len(values) + 1)
    for i in range(len(memo)):
        memo[i] = [-1] * (capacity + 1)
    # initialize the base case
    for i in range(len(memo)):
        memo[i][0] = 0
    for j in range(capacity + 1):
        memo[0][j] = 0
    return knapsack_aux(len(values), capacity, values, weights, memo)


def knapsack_aux(i, j, values, weights, memo):
    if i == 0 or j == 0:
        return memo[i][j]
    else:
        if memo[i][j] == -1:
            max_value = -1
            exclude = knapsack_aux(i - 1, j, values, weights, memo)
            if j >= weights[i - 1]:
                include = values[i - 1] + knapsack_aux(i - 1, j - weights[i - 1], values, weights, memo)
                if exclude >= include:
                    max_value = exclude
                else:
                    max_value = include
            else:
                max_value = exclude
            memo[i][j] = max_value
        return memo[i][j]


if __name__ == '__main__':
    def test_knapsack():
        values, weights = [230, 40, 350, 550], [6, 1, 5, 9]
        assert knapsack_btn(values, weights, 8) == 390
        assert knapsack_btn(values, weights, 10) == 590
        assert knapsack_top(values, weights, 8) == 390
        assert knapsack_top(values, weights, 10) == 590
        print('pass test for 1/0 knapsack')


    test_knapsack()
