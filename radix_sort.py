from counting_sort import *


def radix_sort(num_list, b=10):
    """
    Function that performs radix sort on a list of integers

    Precondition:           input list must only contain positive integer in range [1, 2^64 - 1]
                            input list mist be at least of size 1
                            base must be positive integer in range [2, inf)
    Arguments:              num_list = a list containing positive integers
                            b = base
    Time complexity:        Best case O((N + b)M) where N is the size of the input list, b is the base, M is the maximun number of digits
                            Worst case O((N + b)M) where N is the size of the input list, b is the base, M is the maximum number of digits
    Space complexity:       O((N + b)M) where N is the size of the input/output list and
                            b is the base and M is the maximun number of digits of integer
    Aux space complexity:   O((N + b)M) where N is the size of the output list and
                            b is the base and M is the maximun number of digits of integer
    Return:                 a list in ascending order containing the same element as the input list
    """
    # step 1: create an output list
    output_list = [None] * len(num_list)
    for i in range(len(num_list)):
        output_list[i] = num_list[i]

    # step 2: find the maximum item in the input list
    max_item = 0
    for item in num_list:
        if item > max_item:
            max_item = item

    # step 3: find the maximum column
    col = 0
    while (max_item // (b ** col)) > 0:
        col += 1

    # step 4: loop through each column and sort base on the digit at that column
    for m in range(col):
        # stable count sort the list based on the key
        output_list = counting_sort(output_list, b, m)

    return output_list


def test_radix_sort():
    print('Running test for radix sort')
    lst = [3, 4, 5, 6, 2, 1, 7, 8]
    assert radix_sort(lst) == sorted(lst)
    lst = [5, 7, 4, 8, 3, 9, 1]
    assert radix_sort(lst) == sorted(lst)
    lst = [234, 323, 324, 456, 346, 554]
    assert radix_sort(lst) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_radix_sort()
