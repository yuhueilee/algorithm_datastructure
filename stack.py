'''
Invariant:
 - valid data in the 0 to count - 1 positions
'''


class Stack:

    def __init__(self, max_capacity):
        '''
        Create a stack
        Elements:   array - store the items
                    integer - indicate the number of items in stack
                    integer - indicate the top item in stack

        :param max_capacity:    maximum capacity of the stack
        :pre:                   size must be positive
        :post:                  an instance of Stack is created
        :complexity:            best and worst O(n) where n is the size
        '''
        if max_capacity <= 0:
            raise Exception("The capacity must be positive")
        self.array = [None]*max_capacity
        self.top = -1
        self.count = 0

    def size(self):
        '''
        Return the size of the stack

        :return:        size of the stack
        :pre:           none
        :post:          none
        :complexity:    best and worst O(1)
        '''
        return self.count

    def is_full(self):
        '''
        Check if stack is full or not

        :return:        True if full, False otherwise
        :pre:           none
        :post:          none
        :complexity:    best and worst O(1)
        '''
        return self.count >= len(self.array)

    def is_empty(self):
        '''
        Check if stack is empty or not

        :return:        True if empty, False otherwise
        :pre:           none
        :post:          none
        :complexity:    best and worst O(1)
        '''
        return self.count == 0

    def push(self, new_item):
        '''
        Push the item on top of the stack

        :param new_item:item to be pushed
        :return:        True if pushed, False otherwise
        :pre:           stack must not be full
        :post:          new item being pushed on the stack
        :complexity:    best and worst O(1)
        '''
        if self.is_full():
            raise Exception("stack is full")
        self.top += 1
        self.count += 1
        self.array[self.top] = new_item
        return self.is_full()

    def pop(self):
        '''
        Pop the top item off the stack

        :return:        item being popped
        :pre:           stack must not be empty
        :post:          top item being popped off the stack
        :complexity:    best and worst O(1)
        '''
        if self.is_empty():
            raise Exception("stack is empty")
        popped = self.array[self.top]
        self.top -= 1
        self.count -= 1
        return popped

    def peek(self):
        pass

    def reset(self):
        '''
        Removes all elements from the container

        :return:        none
        :pre:           none
        :post:          none
        :complexity:    best and worst O(1)
        '''
        self.top = -1
        self.count = 0