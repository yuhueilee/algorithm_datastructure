'''
Invariant
 - Program will terminates after looping for N times where N is the length of the input list
 - Program will produce the correct result where items in input list are sorted
   - At k iteration, list[1:k] is sorted
'''


def min_index(lst):
    '''
    Find the index of minimum item in list

    :param lst:     input list
    :return:        index of minimum item
    :time:          best and worst O(N)
    :space:         best and worst O(N) where N is the length of the input list
    '''
    # get index of the smallest item in aList
    index = 0  # initialize index
    for i in range(1, len(lst)):
        if lst[i] < lst[index]:
            index = i  # keep track of the smallest item
    return index


def selection_sort(lst):
    '''
    Sort the input list in ascending order
      - Not stable
      - In-place

    :param lst:     input list
    :return:        sorted input list
    :time:          best and worst O(N^2)
    :space:         best and worst O(N) where N is the length of the input list
    '''
    for i in range(len(lst)):
        index = min_index(lst[i:])
        lst[i], lst[i + index] = lst[i + index], lst[i]
    return lst


def test_selection_sort():
    print('Running test for selection sort')
    lst = [3, 2, 6, 5, 4, 3, 2]
    assert selection_sort(lst) == sorted(lst)
    lst = [3, 2, 5, 2, 4, 7, 4, 3, 5]
    assert selection_sort(lst) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_selection_sort()

