def counting_sort(num_list, b=10, col=0):
    """
    Function that performs stable count sort on list of integers

    Precondition:           input list must only contain positive integer including 0
                            input list mist be at least of size 1
    Arguments:              num_list = a list containing positive integers including 0
                            b = base
                            col = column of the digit to be sorted
    Time complexity:        Best case O(N + M) where N is the size of the input list and M is the size of the count list
                            Worst case O(N + M) where N is the size of the input list and M is the size of the count list
    Space complexity:       O(N + M) where N is the size of the input/output list and M is the size of the count/position list
    Aux space complexity:   O(N + M) where N is the size of the output list and M is the size of the count/position list
    Return:                 a list in ascending order containing the same element as the input list
    """
    # step 1: find the maximum item in the list
    max_item = 0
    for item in num_list:
        item = (item // (b ** col)) % b
        if item > max_item:
            max_item = item

    # step 2: create a count list, a rank list, an output list
    count_list = [0] * (max_item + 1)
    rank_list = [0] * (max_item + 1)
    output_list = [0] * len(num_list)

    # step 3: construct count list
    for item in num_list:
        key = (item // (b ** col)) % b
        count_list[key] += 1

    # step 4: construct rank list
    for i in range(1, len(count_list)):
        rank_list[i] = rank_list[i - 1] + count_list[i - 1]

    # step 5: construct output list
    # go through the input list
    for item in num_list:
        key = (item // (b ** col)) % b
        # set output list at index rank_list[key] to the item in input list
        output_list[rank_list[key]] = item
        # increment rank_list[key] by 1
        rank_list[key] += 1

    # step 6: return the output list
    return output_list


def test_counting_sort():
    print('Running test for counting sort')
    lst = [3, 4, 5, 6, 2, 1, 7, 8]
    assert counting_sort(lst) == sorted(lst)
    lst = [5, 7, 4, 8, 3, 9, 1]
    assert counting_sort(lst) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_counting_sort()
