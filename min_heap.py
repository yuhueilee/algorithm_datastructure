'''
Min Heap (array):
 - root is always smaller than its child
 - essentially complete binary tree
 - duplicate values are allowed

Invariants:
 - tree is a binary tree
 - essentially complete
 - heap sorted
'''


class MinHeap:

    def __init__(self, nodes, max_capacity=100):
        '''
        Min Heap (array representation)

        :param nodes:           a list of nodes to be stored in the min heap
        :param max_capacity:    maximum capacity of the heap
        :space complexity:      best and worst O(V) where V is the number of nodes in min heap
        :aux space complexity:  best and worst O(V) where V is the number of nodes in min heap
        '''
        # store all the nodes, index 0 is None
        self.heap = [None] * (max_capacity + 1)
        # pointer to the index of the last node in heap
        self.rear = 0
        # append the nodes to the heap
        for node in nodes:
            self.append(node)

    def append(self, node):
        '''
        Append the node into min heap
        (Perform up heap)

        :param node:        node that has key as key and dist as value
        :return:            None
        :time complexity:   best and worst O(logV) where V is the number of nodes in min heap
        '''
        # increment the rear
        self.rear += 1
        # assign the index to the node
        node.index = self.rear
        # append to the last position in heap
        self.heap[self.rear] = node
        # rise the node
        self.rise(self.rear)

    def serve(self):
        '''
        Serve the root from min heap
        (Perform down heap)

        :return:            node being served
        :time complexity:   best and worst O(logV) where V is the number of nodes in min heap
        '''
        if self.is_empty():
            raise Exception("Heap is empty")

        res = self.heap[1]
        # swap root with leaf
        self.heap[1], self.heap[self.rear] = self.heap[self.rear], self.heap[1]
        # swap index of root with leaf
        self.heap[1].index, self.heap[self.rear].index = self.heap[self.rear].index, self.heap[1].index
        # decrement rear
        self.rear -= 1
        # sink the node
        self.sink(1)
        return res

    def update(self, node, val):
        '''
        Update the min heap with the new node

        :param node:        node that needs to be updated
        :param val:         value that needs to be updated for the node given
        :return:            None
        :time complexity:   best and worst O(logV) where V is the number of nodes in the min heap
        '''
        # find the index of the node to be updated
        i = node.index
        old_node = self.heap[i]
        # case 1: value increase then sink
        if old_node.value < val:
            self.heap[i].value = val
            self.sink(i)
        # case 2: value decrease then rise
        elif old_node.value > val:
            self.heap[i].value = val
            self.rise(i)
        # case 3: value remain then do nothing
        else:
            pass

    def rise(self, current):
        '''
        Rise the node at index to its correct position in min heap

        :param current:     index of the current node to be risen
        :return:            None
        :time complexity:   best and worst O(logV) where V is the number of nodes in the min heap
        '''
        parent = current // 2
        # current node is not the root and the parent is greater than its child
        while current > 1 and self.heap[parent].value > self.heap[current].value:
            # swap current with parent
            self.heap[current], self.heap[parent] = self.heap[parent], self.heap[current]
            # swap index of current with parent
            self.heap[current].index, self.heap[parent].index = self.heap[parent].index, self.heap[current].index
            parent, current = parent // 2, parent

    def sink(self, current):
        '''
        Sink the node at index to its correct position in min heap

        :param current:       index of the node to be sank
        :return:            None
        :time complexity:   best and worst O(logV) where V is the number of nodes in the min heap
        '''
        while self.min_child(current) is not None and self.heap[current].value > self.heap[
            self.min_child(current)].value:
            k = self.min_child(current)
            # swap current with min child
            self.heap[current], self.heap[k] = self.heap[k], self.heap[current]
            # swap index of current with min chile
            self.heap[current].index, self.heap[k].index = self.heap[k].index, self.heap[current].index
            current = k

    def min_child(self, index):
        '''
        Return the index of the smaller child of the node at index

        :param index:       index of the node
        :return:            index of the smaller child
        :time complexity:   best and worst O(1)
        '''
        left, right = 2 * index, 2 * index + 1
        # if it has two children
        if right <= len(self):
            if self.heap[left].value < self.heap[right].value:
                return left
            else:
                return right
        # if it only has a left child
        elif left <= len(self) < right:
            return left
        else:
            return None

    def is_empty(self):
        '''
        Check if the heap is empty or not

        :return:    true if empty, false otherwise
        '''
        return self.rear == 0

    def __str__(self):
        '''
        Return a string representation of the min heap

        :return:    string representation of the min heap
        '''
        ret_val = ""
        for i in range(1, len(self) + 1):
            ret_val += str(self.heap[i]) + "\n"
        return ret_val

    def __len__(self):
        '''
        Return the length of the min heap

        :return:    length of the min heap
        '''
        return self.rear


class Node:
    def __init__(self, key, value):
        '''
        Node store in the heap

        :param key:     key of node
        :param value:   value of node
        '''
        self.key = key
        self.value = value
        # represents the index position in heap
        self.index = None

    def __str__(self):
        '''
        Return a string representation of the node

        :return:    string representation of the node
        '''
        return "Vertex id=" + str(self.key) + " dist=" + str(self.value) + " index=" + str(self.index)
