from graph import *


class Graph(Graph):
    def bfs(self, source):
        '''
        Breadth-First Search (single source, multiple targets) for unweighted graph
            - Reachability
            - Shortest path (non-negative edges)

        :param source:          source vertex
        :return:                list of vertices in bfs sequence
        :time complexity:       best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :space complexity:      best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :aux space complexity:  best and worst O(V+E) where V is the number of vertices and E is the number of edges
        '''
        # reset the graph
        self.reset()
        # create a list to store visited vertices
        visited = []
        # create a queue to store discovered vertices
        discovered = Queue(len(self))
        # append source vertex to discovered
        source = self.vertices[source]
        discovered.append(source)
        # mark it as discovered
        source.discovered = True

        # traverse through the graph
        while not discovered.is_empty():
            u = discovered.serve()  # serve from the queue
            u.visited = True  # mark it as visited
            visited.append(u)  # append to visited list

            # traverse through the neighbors of the current vertex u
            for e in u.edges:
                v = e.v
                v = self.vertices[v]
                # check if neighbor discovered and visited or not
                if not v.discovered and not v.visited:
                    discovered.append(v)  # append to the queue
                    v.discovered = True  # mark it as discovered

        return visited


if __name__ == '__main__':
    def test_graph(n, edges):
        # create a graph
        # vertex id = 0, 1, 2, ..., n
        my_graph = Graph(n)
        print('initialize the graph:\n', my_graph)
        my_graph.insert_edges(edges)
        print('inserting edges to the graph:\n', my_graph)

        return my_graph


    # bfs
    def test_bfs(graph):
        print('BFS:')
        for i in range(len(graph)):
            print(str(i) + " infected " + str(len(graph.bfs(i))))


    # create a list of edges
    edges = []
    edges.append((0, 1, 5))  # u = 0, v = 1, w = 5
    edges.append((0, 2, 12))  # u = 0, v = 2, w = 12
    edges.append((1, 2, 6))  # u = 1, v = 2, w = 6
    edges.append((2, 3, 1))  # u = 2, v = 3, w = 1
    edges.append((3, 4, 7))  # u = 3, v = 4, w = 7
    edges.append((4, 1, 3))  # u = 4, v = 1, w = 3
    my_graph = test_graph(5, edges)
    test_bfs(my_graph)
