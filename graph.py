from queue import *
from min_heap import *


class Graph:
    '''
    Graph with adjacency list representation and is connected, undirected and simple
    '''

    def __init__(self, n):
        '''
        Create a graph by reading the input file

        :param n:               number of vertices
        :time complexity:       best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :space complexity:      best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :aux space complexity:  best and worst O(V+E) where V is the number of vertices and E is the number of edges
        '''
        self.vertices = [None] * n
        for i in range(n):
            self.vertices[i] = Vertex(i)

    def insert_edges(self, edges, directed=True):
        '''
        Insert edges to vertices in graph

        :param edges:           list of tuples containing u, v, w
        :param directed:        boolean value indicate if graph is directed or not
        :time complexity:       best and worst O(E) where E is the number of edges in the list
        :space complexity:      best and worst O(E) where E is the number of edges in the list
        :aux space complexity:  best and worst O(E) where E is the number of edges in the list
        '''
        for e in edges:
            u, v, w = e[0], e[1], e[2]
            # add u to v
            self.vertices[u].insert_edge(Edge(u, v, w))
            if not directed:
                # add v to u
                self.vertices[v].insert_edge(Edge(v, u, w))

    def reset(self):
        '''
        Reset the properties of the vertices in the graph after traversal

        :time complexity:       best and worst O(V) where V is the number of vertices
        '''
        for v in self.vertices:
            v.visited = False
            v.discovered = False
            v.dist = 0

    def __len__(self):
        '''
        Return the number of vertices in graph

        :return:    integer number of vertices
        '''
        return len(self.vertices)

    def __str__(self):
        '''
        Return a string representation of the graph

        :return:    string representation of graph
        '''
        ret_val = ""
        for i in range(len(self.vertices)):
            ret_val += str(self.vertices[i]) + "\n"
        return ret_val


class Vertex:
    '''
    V = (E)
    Vertex contains a list of edges (adjacency list)
    '''

    def __init__(self, id, dist=0):
        '''
        Each vertex has an id

        :param id:              integer id of the vertex
        :param dist:            integer distance of the vertex
        :time complexity:       best and worst O(1) to assign the id
        :space complexity:      best and worst O(E) where E is the number of edges in the list
        :aux space complexity:  best and worst O(E) where E is the number of edges in the list
        '''
        # id of the vertex
        self.id = id
        # list of edges of the vertex
        self.edges = []
        # visited
        self.visited = False
        # discovered
        self.discovered = False
        # distance
        self.dist = dist

    def insert_edge(self, edge):
        '''
        Insert edge to the edges list of the vertex

        :param edge:            Edge object
        :time complexity:       best and worst O(1) to append to the edges list
        :space complexity:      best and worst O(E) where E is the number of edges in the list
        :aux space complexity:  best and worst O(E) where E is the number of edges in the list
        '''
        self.edges.append(edge)

    def __str__(self):
        '''
        Return a string representation of the vertex

        :return:    string representation of vertex
        '''
        ret_val = ""
        ret_val += "Vertex " + str(self.id)
        for e in self.edges:
            ret_val += "\n with edge " + str(e)
        ret_val += "\n with distance " + str(self.dist)
        return ret_val


class Edge:
    '''
    E = (u, v, w)
    Edge contains source vertex, target vertex, and the weight of the edge
    '''

    def __init__(self, u, v, w):
        '''
        Each edge has vertices and weight

        :param u:   source vertex id
        :param v:   neighbor vertex id
        :param w:   weight of the edge
        :time complexity:       best and worst O(1)
        :space complexity:      best and worst O(1)
        :aux space complexity:  best and worst O(1)
        '''
        self.u = u
        self.v = v
        self.w = w

    def __str__(self):
        '''
        Return a string representation of the edge

        :return:    string representation of the edge
        '''
        return str(self.u) + ', ' + str(self.v) + ', ' + str(self.w)
