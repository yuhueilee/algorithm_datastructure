# Algorithm and Data Structure
  - This is used to store the algorithms and common data structures used

## Algorithm
  - Sorting algorithms
    - Selection Sort
    - Insertion Sort
    - Heap Sort
    - Merge Sort
    - Quick Sort
      - Lumoto's (in-place)
      - Hoare's (in-place)
      - Out-of-place
  - Dynamic Programming
    - Fibonacci
    - Coin Change
    - Knapsack
    - Edit Distance
    - House Profit
    - Longest Increasing Subsequence (LIS)
    - Longest Common Subsequence (LCS) 
    - Minimum Sum Partition
    - Maximum Subarray
  - Graph Traversal
    - BFS
    - DFS
  - Shortest Path 
    - Breadth-First Search
    - Dijsktra's
    - Bellman-Ford
    - Floyd-Warshall (x)
  - Minimum Spanning Tree
    - Prim's
    - Kruskal's

## Data Structure
  - Queue
    - Circular Queue
    - Priority Queue
      - Min Heap
      - Max Heap
  - Stack
  - Trie
    - Prefix Trie
    - Suffix Trie (x)
    - Suffix Array (x)
  - Graph

