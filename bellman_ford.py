from graph import *
import math


class Graph(Graph):

    def bellmanford(self, source):
        '''
        Bellman Ford algorithm (single source, multiple targets) for weighted and directed graph
            - Shortest path (negative edges)
            - Detect negative cycle

        :param source:          source vertex
        :return:                shortest path from source to all the reachable vertices
        :raise Exception:       when negative-weight cycle is deteced
        :time complexity:       best and worst O(VE) where V is the number of vertices and E is the number of edges
        :space complexity:      best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :aux space complexity:  best and worst O(V+E) where V is the number of vertices and E is the number of edges
        '''
        # reset the graph
        self.reset()
        # create a list to store the predecessors
        pred = [None] * len(self)
        # initialize predecessor of source to be null
        pred[source] = None

        # initialize the distance to be inf except the source vertex
        for id in range(len(self)):
            if id != source:
                self.vertices[id].dist = math.inf

        # loop for V-1 iterations O(V)
        for k in range(len(self) - 1):
            print('k=', k)
            for i in range(len(self)):
                print(self.vertices[i].dist)
            # traverse through every edges in graph O(V + E)
            for i in range(len(self)):
                u = self.vertices[i]
                # relax the edge
                for e in u.edges:
                    v = e.v
                    v = self.vertices[v]
                    # update the distance
                    if v.dist > u.dist + e.w:
                        v.dist = u.dist + e.w
                        # update pred
                        pred[v.id] = u.id

        # check for negative-weight cycle O(V + E)
        for i in range(len(self)):
            u = self.vertices[i]
            # relax the edge
            for e in u.edges:
                v = e.v
                v = self.vertices[v]
                # check if the distance decreases
                if v.dist > u.dist + e.w:
                    print("negative-weight cycle is found")
                    raise Exception

        return pred


if __name__ == '__main__':
    def test_graph(n, edges):
        # create a graph
        # vertex id = 0, 1, 2, ..., n
        my_graph = Graph(n)
        print('initialize the graph:\n', my_graph)
        my_graph.insert_edges(edges)
        print('inserting edges to the graph:\n', my_graph)

        return my_graph


    # bellman-ford
    def test_bellmanford(graph):
        print('\nBellman-Ford:')
        try:
            print(graph.bellmanford(0))
        except Exception:
            pass


    # create a list of edges
    edges = []
    edges.append((0, 1, 5))  # u = 0, v = 1, w = 5
    edges.append((0, 2, 12))  # u = 0, v = 2, w = 12
    edges.append((1, 2, 6))  # u = 1, v = 2, w = 6
    edges.append((2, 3, 1))  # u = 2, v = 3, w = 1
    edges.append((3, 4, 7))  # u = 3, v = 4, w = 7
    edges.append((4, 1, 3))  # u = 4, v = 1, w = 3
    my_graph = test_graph(5, edges)
    # create a new edge
    edges.pop()
    edges.append((4, 1, -15))  # u = 4, v = 1, w = -15
    my_second_graph = test_graph(5, edges)
    # create a new graph/ tutorial w10 q2
    edges = []
    edges.append((0, 1, 5))
    edges.append((0, 2, 6))
    edges.append((1, 3, -1))
    edges.append((2, 1, 4))
    edges.append((2, 4, -10))
    edges.append((3, 2, -2))
    edges.append((3, 4, 4))
    edges.append((3, 5, 3))
    edges.append((4, 6, -6))
    edges.append((5, 4, -5))
    edges.append((6, 5, 10))
    my_third_graph = test_graph(7, edges)
    # create another new graph/ tutorial w10 q6
    edges = []
    edges.append((1, 2, 3))
    edges.append((2, 3, 2))
    edges.append((3, 4, -5))
    edges.append((4, 2, 1))
    edges.append((2, 5, 2))
    edges.append((0, 1, 0))
    edges.append((0, 2, 0))
    edges.append((0, 3, 0))
    edges.append((0, 4, 0))
    edges.append((0, 5, 0))
    my_fourth_graph = test_graph(6, edges)

    test_bellmanford(my_second_graph)
    test_bellmanford(my_third_graph)
    test_bellmanford(my_fourth_graph)
