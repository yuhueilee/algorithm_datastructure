from graph import *
import math


class Graph(Graph):

    def dijkstra(self, source):
        '''
        Dijkstra's algorithm (single source, multiple targets) for weighted and directed graph
            - Shortest path (non-negative edges)

        :param source:          source vertex
        :return:                predecessor list which has the shortest path from source to all the reachable vertices
        :time complexity:       best and worst O(ElogV) where V is the number of vertices and E is the number of edges
        :space complexity:      best and worst O(V+E) where V is the number of vertices and E is the number of edges
        :aux space complexity:  best and worst O(V+E) where V is the number of vertices and E is the number of edges
        '''
        # no need to reset the graph as this performs on the preprocess graph only
        # create a list to store the predecessors
        pred = [None] * len(self)
        # create a nodes list
        nodes = []
        for i in range(len(self)):
            # mark the distance of source vertex to be 0
            if i == source:
                self.vertices[i].dist = 0
            else:
                self.vertices[i].dist = math.inf
            vertex = self.vertices[i]
            nodes.append(Node(vertex.id, vertex.dist))
        # create a priority queue (min heap) to store discovered vertices
        discovered = MinHeap(nodes, len(self))
        # initialize predecessor of source to be null
        pred[source] = None
        # mark it as discovered
        self.vertices[source].discovered = True

        # traverse through the graph
        while not discovered.is_empty():
            node = discovered.serve()  # serve from the heap
            u_id = node.id
            u = self.vertices[u_id]
            u.visited = True  # mark it as visited

            # traverse through the edges
            for e in u.edges:
                # perform relaxation
                self.relax(e, pred, discovered, nodes)

        return pred

    def relax(self, e, pred, heap, nodes):
        '''
        Perform relaxation on the edge

        :param e:               edge of the vertex
        :param pred:            predecessor list
        :param heap:            min heap that stores the id and distance of each vertex
        :param nodes:           a list of nodes stored in the heap
        :return:                None
        :time complexity:       best and worst O(logV) where V is the number of vertices
        '''
        u_id, v_id, w = e.u, e.v, e.w
        u, v = self.vertices[u_id], self.vertices[v_id]
        node = nodes[v_id]
        # Neutral
        if not v.visited:
            if not v.discovered:
                v.discovered = True  # mark it as discovered
                heap.update(node, u.dist + w)  # update the heap
                v.dist = u.dist + w  # update dist
                pred[v_id] = u_id  # update pred
            # Discovered
            else:
                # update the dist
                if v.dist > u.dist + w:
                    heap.update(node, u.dist + w)  # update the heap
                    v.dist = u.dist + w  # update dist
                    pred[v_id] = u_id  # update pred


if __name__ == '__main__':
    def test_graph(n, edges):
        # create a graph
        # vertex id = 0, 1, 2, ..., n
        my_graph = Graph(n)
        print('initialize the graph:\n', my_graph)
        my_graph.insert_edges(edges)
        print('inserting edges to the graph:\n', my_graph)

        return my_graph


    # dijkstra's
    def test_dijkstra(graph):
        print('\nDijkstra from source 0:')
        pred = graph.dijkstra(0)
        for vertex in pred:
            print(vertex)
        print('\nDijkstra from source 4:')
        pred = graph.dijkstra(4)
        for vertex in pred:
            print(vertex)


    # create a list of edges
    edges = []
    edges.append((0, 1, 5))  # u = 0, v = 1, w = 5
    edges.append((0, 2, 12))  # u = 0, v = 2, w = 12
    edges.append((1, 2, 6))  # u = 1, v = 2, w = 6
    edges.append((2, 3, 1))  # u = 2, v = 3, w = 1
    edges.append((3, 4, 7))  # u = 3, v = 4, w = 7
    edges.append((4, 1, 3))  # u = 4, v = 1, w = 3
    my_graph = test_graph(5, edges)
    test_dijkstra(my_graph)
