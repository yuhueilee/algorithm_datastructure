def quick_sort(lst):
    '''
    Sort the input list in ascending order
      - Not stable
      - Not in-place

    :param lst:         input list
    :return:            sorted input list
    :time:              best O(NlogN) and worst O(N^2) where N is the length of the list
    :space:             best and worst O(N) where N is the length of the list
    '''
    start, end = 0, len(lst) - 1
    quick_sort_aux(lst, start, end)
    return lst


def quick_sort_aux(lst, start, end):
    if start < end:
        # call partition
        boundry = partition(lst, start, end)
        # call auxiliary function
        quick_sort_aux(lst, start, boundry - 1)
        quick_sort_aux(lst, boundry + 1, end)


def partition(lst, start, end):
    # create left and right list
    left, right = [], []
    # choose a pivot
    index = (start + end) // 2
    pivot = lst[index]
    # initialize the boundry
    boundry = start
    for i in range(start, end+1):
        if i != index:
            if lst[i] <= pivot:
                left.append(lst[i])
                boundry += 1
            else:
                right.append(lst[i])
    # overwrite to the input list
    lst[start:end+1] = left + [pivot] + right
    return boundry


def test_quick_sort():
    print('Running test for quick sort')
    lst = [3, 4, 5, 6, 2, 1, 7, 8]
    assert quick_sort(lst) == sorted(lst)
    lst = [5, 7, 4, 8, 3, 9, 1]
    assert quick_sort(lst) == sorted(lst)
    print('Pass all tests')


if __name__ == '__main__':
    test_quick_sort()

